﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel; //used for ObservableCollection
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace naviPageV2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OfficePage : ContentPage
    {
        //list of employees 
        ObservableCollection<string> employees = new ObservableCollection<string>();

        public OfficePage()
        {
            InitializeComponent();
        }

        //adds user input to list of employees
        public void Handle_AddButtonPressed(object sender, EventArgs e)
        {
            employees.Add(entryText.Text);
            theEmpList.ItemsSource = employees;
        }
        //removes last entry from list of employees
        public void Handle_RemoveButtonPressed(object sender, EventArgs e)
        {
            if (employees.Count > 0)
            {
                employees.RemoveAt(employees.Count - 1);
                theEmpList.ItemsSource = employees;
            }

        }
    }
}