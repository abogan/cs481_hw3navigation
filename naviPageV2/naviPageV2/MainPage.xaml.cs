﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace naviPageV2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_NavigateToCalculator(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CalcPage());
        }
        async void Handle_NavigateToOffice(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new OfficePage());
        }
        async void Handle_NavigateToTapASquare(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SquarePage());
        }
    }
}
